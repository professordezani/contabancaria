abstract class Conta
{
    // atributos
    protected decimal saldo;
    public readonly Cliente Cliente;

    // construtor:
    public Conta(Cliente cliente) 
    {
        this.Cliente = cliente;
    }

    // métodos
    public decimal GetSaldo()
    {
        return this.saldo;
    }

    public void Creditar(decimal valor)
    {
        this.saldo += valor;
    }

    public virtual void Debitar(decimal valor)
    {
        // Fail Fast
        if(this.saldo < valor)
            throw new Exception("Saldo insuficiente");

        this.saldo -= valor;
    }

    // public abstract void Transferir(decimal valor);
}